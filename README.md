### FX Spot Price Steamer
This is a React 18 front end with C# .NET 8 backend for streaming FX spot prices. A screenshot of the interface is provided below. I have left comments in the code to explain my working / thinking and left functionality which is somewhat demonstrative to illustrate what I may do in another scenario.

![](screenshot.png)

## How to run:
Requires either [.NET 8, npm and SQL Server] or Docker. You may need to install dev certificates to use https.

<u>**Option 1**</u>
- Run front end with

		npm install
		npm run dev

- Run the back end with:
		dotnet restore "./FxSpotStreamer.App/FxSpotStreamer.App.csproj"
		dotnet build "./FxSpotStreamer.App/FxSpotStreamer.App.csproj" -c Release -o ./app/build
		dotnet publish "./FxSpotStreamer.App/FxSpotStreamer.App.csproj" -c Release -o ./app/publish /p:UseAppHost=false
		dotnet "./app/publish/FxSpotStreamer.App.dll"
	
- Run database with the associated dockerfile or run the setup.sql file in an existing SQL Server.

<u>**Option 2**</u>
Run the docker-compose file via the command "docker compose up" which will start up a SQL server, front end and back end

<u>**Option 3**</u>
Run the run.bat file directly which will start up a SQL server, front end and back end

Visit http://localhost:5173

## UI - React 18 with Vite, AG Grid and SignalR:
- AG Grid as a simple and optimised framework for tabulating data - specifically well versed for processing visuals on high data volumes.
- SignalR is a well established protocol for bidirectional client-server communication, enabling pushing of data from server to UI when data is received. I have also provided a poll based alternative via the "useLiveFxSpotPriceApi" hook for demonstration purposes. The SignalR functionality is provided via a hook with dependency injection to allow the connection to be mocked quite easily during testing. I have left a test in the solution to demonstrate how I would test the SignalR hook.
- Vite is a tool which provides very good performance and a lot of autonomy over the React solution (EG not bound by configurations as we are with create react app, which is now a deprecated solution anyway).
- Created buttons using styled components rather than MUI, Bootstrap as required by the problem. Used colours matching the AG Grid.
- Did not use any shared state frameworks (redux, recoil) due to the fact the problem does not require such.

## Server - .NET 8 with SignalR and EF Core
- EF Core has been used to demonstrate communication between a server and data storage. 
- MarketDataEventManager has been provided to mimic an event manager such as Kafka, EventHub or a MQL protocol. The ListenForPriceUpdatesAsync would be where you process an event when received. Pub/Sub pattern.
- Provided both an API and SignalR connection to demonstrate ability to provide both push and pull style services.
- Use of standard design principles and patterns including dependency injection with keyed services to ensure testability and simple extendability. Would use something like NSubstitute or Moq for unit and integration testing.

## Improvements to be made
- Add authentication layer with OAuth and OIDC (or whichever authentication protocol is available). On the server side, I would put it in the middleware. On the front end, SignalR supports token authentication on the initial connection.
- Create a more robust connection according to more explicit rules - i.e. what are the timeouts, should actions be re-tried, should the application terminate on certain unexpected behaviours, etc etc.
- Provide a permissioning feature to allow users to customise their views
- Do not leave secret information in configuration or files - would put them in ENV variables. Done in config and files purely for simplicity.
- Add some form of automation (triggers or something of that nature) on the database to automatically historise the prices in case history is needed for debugging or user purposes
