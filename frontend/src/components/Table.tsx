import { useEffect, useState } from 'react';
import { AgGridReact } from "ag-grid-react";
import { ColDef, ColGroupDef } from "ag-grid-community/dist/types/core/entities/colDef";
import "ag-grid-community/styles/ag-grid.css"; // Mandatory CSS required by the grid
import "ag-grid-community/styles/ag-theme-quartz.css";
import { GetRowIdParams } from "ag-grid-community/dist/types/core/interfaces/iCallbackParams";

export interface Props<T> {
    data: T[] | null;
    columns: (ColDef<T> | ColGroupDef<T>)[] | null;
}

// use this to set any specific default column settings
function updateColumnDefs<T>(columns: (ColDef<T> | ColGroupDef<T>)[] | null): (ColDef<T> | ColGroupDef<T>)[] | null  {
    if (!columns) {
        return null;
    }

    return columns.map((column: (ColDef<T> | ColGroupDef<T>)) => {
        return {
            ...column,
            maxWidth: 200,
        }
    });
}

export function Table<T>({ data, columns }: Props<T>) {
    const [rowData, setData] = useState<T[] | null>();
    const [colDefs] = useState<(ColDef<T> | ColGroupDef<T>)[] | null>(updateColumnDefs(columns));

    useEffect(() => {
        setData(data);
    }, [data]);

    // using get row id enforces the AG Grid to optimise updates and ensures the order of the items in the grid.
    function getRowId(params: GetRowIdParams<T>) {
        return params.data.id;
    }
    return (
        <div className="ag-theme-quartz-dark">
            <AgGridReact domLayout="print" getRowId={getRowId} rowData={rowData} columnDefs={colDefs} />
        </div>
    );
}
