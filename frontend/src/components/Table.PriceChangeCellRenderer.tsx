import { CustomCellRendererProps } from "ag-grid-react";
import styled from "styled-components";

const InlineDiv = styled.div<{ direction: number }>`
  display: inline-block;
  color: ${( { direction }) => (direction === 0 ? "red" : (direction === 1 ? "green" : "white"))};
`;

export default (params: CustomCellRendererProps) => {
    // use toLocaleString to truncate the decimals at the last zero and format the number according to the user's local
    // setting.
    return (
        <>
            {params.data["lastUpdateDirectionUp"] === 0 ?
                <InlineDiv direction={params.data["lastUpdateDirectionUp"]}>{String.fromCharCode(8595)}</InlineDiv> :
                params.data["lastUpdateDirectionUp"] === 1 ?
                    <InlineDiv direction={params.data["lastUpdateDirectionUp"]}>{String.fromCharCode(8593)}</InlineDiv> :
                    <InlineDiv direction={params.data["lastUpdateDirectionUp"]}>{String.fromCharCode(8596)}</InlineDiv>
            }
            {params.data["price"]?.toLocaleString()}
        </>
    )
}
