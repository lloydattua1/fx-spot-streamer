import { HubConnection, HubConnectionBuilder } from "@microsoft/signalr";
import { useEffect, useState } from "react";
import { WebsocketConnected, WebsocketDisconnected } from "../domain/ConnectionStatus";
import { FxSpotPrice } from "../domain/FxSpotPrice";
import { LiveFxSpotPriceHubData } from "./liveFxSpotPriceHubData";
import appSettingsJson from "./../assets/appsettings.json";

export function useLiveFxSpotPriceHub(isConnected: boolean, fxSpotPriceHubConnection?: HubConnection): LiveFxSpotPriceHubData {
    const [signalRHubConnection, setConnection] = useState<HubConnection>();
    const [hubState, setHubState] = useState<string>(WebsocketDisconnected);
    const [prices, setPrices] = useState<FxSpotPrice[] | null>(null);

    useEffect(() => {
        const hubConnection: HubConnection = fxSpotPriceHubConnection ?? new HubConnectionBuilder()
            .withUrl(appSettingsJson.liveFxSpotPricesService,{
            })
            .withAutomaticReconnect()
            .build();

        const onClose = (error?: Error): void => {
            if (error) {
                console.error(`${Date.now()}: Error on close`, error);
            }
            console.log(`${Date.now()}: Connection closed`);
            setHubState(WebsocketDisconnected);
        }
        const onReconnected = (connectionId?: string): void => {
            console.log(`${Date.now()}: Connection established`);
            setHubState(WebsocketConnected);
        }
        const onReconnecting = (error?: Error): void => {
            console.log(`${Date.now()}: Reconnecting...`);
            if (error) {
                console.error(`${Date.now()}: Error on reconnect`, error);
            }
            setHubState(WebsocketDisconnected);
        }

        hubConnection.onclose(onClose)
        hubConnection.onreconnected(onReconnected);
        hubConnection.onreconnecting(onReconnecting);
        hubConnection.on("priceChangedEvent", (newPrices: FxSpotPrice[]) => {
            setPrices(newPrices);
        });
        setConnection(hubConnection);

        // Clean up connection on disposal of the view.
        return () => {
            hubConnection
                .stop()
                .then(() => console.log("Session terminated"))
                .catch(error => console.error(error));
        };
    }, []);

    useEffect(() => {
        if (signalRHubConnection) {
            if (isConnected) {
                signalRHubConnection
                    .start()
                    .then(() => setHubState(WebsocketConnected))
                    .catch(error => console.error(error));
            } else {
                signalRHubConnection
                    .stop()
                    .catch(error => console.error(error));
            }
        }
    }, [signalRHubConnection, isConnected]);

    return { hubState, prices };
}
