import { FxSpotPrice } from "../domain/FxSpotPrice";

export interface LiveFxSpotPriceHubData {
    hubState: string;
    prices: FxSpotPrice[] | null;
}
