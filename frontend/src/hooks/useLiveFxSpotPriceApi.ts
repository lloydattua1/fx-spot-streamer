import axios from "axios";
import { useEffect, useRef, useState } from "react";
import { WebsocketConnected, WebsocketDisconnected } from "../domain/ConnectionStatus";
import { FxSpotPrice } from "../domain/FxSpotPrice";
import { LiveFxSpotPriceHubData } from "./liveFxSpotPriceHubData";
import appSettingsJson from "./../assets/appsettings.json";

export function useLiveFxSpotPriceApi(isConnected: boolean): LiveFxSpotPriceHubData {
    const [intervalTimeout, setIntervalTimeout] = useState<NodeJS.Timeout>();
    const [hubState, setHubState] = useState<string>(WebsocketDisconnected);
    const [prices, setPrices] = useState<FxSpotPrice[] | null>(null);
    const connected = useRef<boolean>(isConnected);

    const refreshPeriod = 1000;

    const getFxPriceData = async () => {
        // Carry out multiple checks on isConnected. The first check determines whether isConnected was changed between
        // the last setTimeout and the execution of this action. The second check determines whether isConnected was
        // changed between the API request and the setting of state. Can happen if we have a long running API request.
        if (connected.current) {
            const response = await axios.get(appSettingsJson.liveFxSpotPriceApi);
            if (connected.current) {
                setPrices(response.data);
                const timeout = setTimeout(getFxPriceData, refreshPeriod);
                setIntervalTimeout(timeout);
            }
        }
    };

    useEffect(() => {
        // Clean up on component unmount
        return () => {
            clearTimeout(intervalTimeout);
        }
    }, []);

    useEffect(() => {
        connected.current = isConnected;
        if (isConnected) {
            getFxPriceData()
                .then(() => setHubState(WebsocketConnected));
        } else {
            clearTimeout(intervalTimeout);
            setHubState(WebsocketDisconnected);
        }

    }, [isConnected]);

    return { hubState, prices };
}
