export interface FxSpotPrice {
    id: number;
    pair: string;
    price: number;
    lastUpdate: string;
    lastUpdateDirectionUp: number;
}
