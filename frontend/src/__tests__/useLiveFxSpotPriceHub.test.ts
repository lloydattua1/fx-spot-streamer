import { renderHook, waitFor } from "@testing-library/react";
import { HubConnection, HubConnectionState } from "@microsoft/signalr";
import { WebsocketConnected } from "../domain/ConnectionStatus";
import { useLiveFxSpotPriceHub } from "../hooks/useLiveFxSpotPriceHub";

// I've left one unit test in place to demonstrate how to test signalR and/or hooks
describe('useLiveFxSpotPriceHub tests', () => {

    beforeEach(() => {
        // this is needed when testing hooks as it removes the timers used to wait for useState updates (which do not
        // have guaranteed run times).
        jest.useFakeTimers();
    })

    const start = jest.fn().mockImplementation(() => Promise.resolve());
    const stop = jest.fn().mockImplementation(() => Promise.resolve());
    const hubConnectionMock = {
        state: HubConnectionState.Disconnected,
        start ,
        stop,
        onclose: jest.fn(),
        onreconnected: jest.fn(),
        onreconnecting: jest.fn(),
        on: jest.fn(),
    } as unknown as HubConnection;

    it('signalR connection should be started and hook should return connection state', async () => {
        const { result } = renderHook(() => useLiveFxSpotPriceHub(true, hubConnectionMock));
        await waitFor(() => {
            expect(result.current.hubState).toBe(WebsocketConnected);
        });
        expect(start).toHaveBeenCalledTimes(1);
    });
});
