import { CellClassParams, ColDef, ColGroupDef } from "ag-grid-community/dist/types/core/entities/colDef";
import { ComponentType, useState } from "react";
import moment from "moment";
import styled from "styled-components";
import { Table } from "./../components/Table";
import { FxSpotPrice } from "../domain/FxSpotPrice";
import { useLiveFxSpotPriceHub } from "../hooks/useLiveFxSpotPriceHub";
import { WebsocketConnected } from "../domain/ConnectionStatus";
import TablePriceChangeCellRenderer from "../components/Table.PriceChangeCellRenderer";

// Could have just used Tailwind or css framework, but wanted to display ability / knowledge of
// styled components
const Button = styled.button.withConfig({
    shouldForwardProp: (prop) =>
    !['btnDisabled'].includes(prop),
})`
  background-color: color-mix(in srgb, transparent, #2196f3 20%);
  color: white;
  border: 2px solid black;
  border-radius: 10px;
  cursor: pointer;
  font-size:0.8rem;
  padding: 0.6rem;
  margin-bottom: 10px;
  opacity: ${( { btnDisabled }) => btnDisabled ? 30 : 100}%;
`;

const LeftMarginedButton = styled(Button)`
  margin-left: 5px;
`;

const LiveFxSpotView: ComponentType = () => {
    const [connected, setConnected] = useState<boolean>(true);
    const liveStockPrices = useLiveFxSpotPriceHub(connected);

    const data: FxSpotPrice[] | null = liveStockPrices.prices?.map((thisLivePrice: FxSpotPrice) => {
        const newPrice = { ...thisLivePrice };
        newPrice.lastUpdate = moment(newPrice.lastUpdate).format("YYYY-MM-DD hh:mm:ss");
        return newPrice;
    });

    const columnDefs: (ColDef<FxSpotPrice> | ColGroupDef<FxSpotPrice>)[] = [
        { field: "id" },
        { field: "pair" },
        { field: "price",
          cellRenderer: TablePriceChangeCellRenderer,
          cellClassRules: {
            'cell-flash-up': (params: CellClassParams<FxSpotPrice, any>) => params?.data["lastUpdateDirectionUp"] === 0,
            'cell-flash-down': (params: CellClassParams<FxSpotPrice, any>) => params?.data["lastUpdateDirectionUp"] === 1,
          }},
        { field: "lastUpdate" }
    ];

    return (
        <>
            <Button
                btnDisabled={liveStockPrices.hubState === WebsocketConnected}
                onClick={(_) => setConnected(true)}>
                {liveStockPrices.hubState === WebsocketConnected ? "Connected" : "Connect"}
            </Button>
            <LeftMarginedButton
                btnDisabled={liveStockPrices.hubState !== WebsocketConnected}
                onClick={(_) => setConnected(false)}>
                {liveStockPrices.hubState === WebsocketConnected ? "Disconnect" : "Disconnected"}
            </LeftMarginedButton>
            <Table<FxSpotPrice> data={data} columns={columnDefs}></Table>
        </>
    )
}

export default LiveFxSpotView
