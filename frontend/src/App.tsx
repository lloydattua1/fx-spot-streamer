import './App.css'
import LiveFxSpotView from "./views/LiveFxSpotView";

function App() {
  return (
      <LiveFxSpotView />
  )
}

export default App
