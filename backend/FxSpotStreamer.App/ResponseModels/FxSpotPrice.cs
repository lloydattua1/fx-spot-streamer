﻿namespace FxSpotStreamer.App.ResponseModels
{
    // Use a separate model for the API response in case you want to alter the response type being sent to the UI or other systems
    public class FxSpotPrice
    {
        public int Id { get; set; }
        public string Pair { get; set; }
        public decimal Price { get; set; }
        public DateTime LastUpdate { get; set; }
        public FxSpotPriceUpdateEnum LastUpdateDirectionUp { get; set; }

        public FxSpotPrice(int id, string pair, decimal price, DateTime lastUpdate, FxSpotPriceUpdateEnum lastUpdateDirectionUp)
        {
            Id = id;
            Pair = pair;
            Price = price;
            LastUpdate = lastUpdate;
            LastUpdateDirectionUp = lastUpdateDirectionUp;
        }
    }
}