﻿namespace FxSpotStreamer.App.ResponseModels
{
    public enum FxSpotPriceUpdateEnum
    {
        Up,
        Down,
        None
    }
}