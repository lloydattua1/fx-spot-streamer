using Microsoft.EntityFrameworkCore;
using FxSpotStreamer.Common.Data;
using FxSpotStreamer.Core;
using FxSpotStreamer.Data;
using FxSpotStreamer.Data.DataStore;
using FxSpotStreamer.App.Mappings;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

builder.Services.AddDbContext<DataContext>(option =>
    option.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

builder.Services.AddKeyedScoped<IStaticFxSpotDataStore, FxSpotDataStore>("liveDataStore");
builder.Services.AddKeyedScoped<IStaticFxSpotDataStore, StaticFxSpotDataStore>("staticDataStore");
builder.Services.AddSingleton<IMarketDataProvider, MarketDataProvider>(); // Use as singleton for caching the data
builder.Services.AddSingleton<IMarketDataEventManager, MarketDataEventManager>(); // Use as singleton for caching the historical values in the simulation

builder.Configuration.AddJsonFile($"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}/appsettings.json", optional: false, reloadOnChange: true);
builder.Services.Configure<DataStoreConfig>(builder.Configuration.GetSection("DataStorage"));

builder.Services.AddAutoMapper(typeof(LiveFxSpotPriceMappingsProfile));

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddCors(options =>
{
    options.AddPolicy("FxSpotPolicy", builder =>
    {
        builder
            .WithOrigins("http://localhost:5173")
            .AllowAnyHeader()
            .WithMethods("POST")
            .AllowCredentials();
    });
});


builder.Services.AddSignalR();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseRouting();
app.UseCors("FxSpotPolicy");
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.MapHub<FxSpotPriceHub>("/liveFxSpotPrices");

// Wait and make sure the service has been initialised
var marketDataService = app.Services.GetService<IMarketDataProvider>();
Task.Run(async () => await marketDataService.InitiateListenerAsync()).Wait();

app.Run();
