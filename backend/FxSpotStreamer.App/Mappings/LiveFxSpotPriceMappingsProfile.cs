﻿using AutoMapper;
using FxSpotStreamer.Common.Domain;
using FxSpotStreamer.Data.Entities;

namespace FxSpotStreamer.App.Mappings
{
    public class LiveFxSpotPriceMappingsProfile : Profile
    {
        public LiveFxSpotPriceMappingsProfile() 
        {
            CreateMap<LiveFxSpotPrice, FxSpotPrice?>().ConvertUsing(ConvertLiveFxSpotPriceEntityToDomain);
            CreateMap<FxSpotPrice?, ResponseModels.FxSpotPrice? > ().ConvertUsing(ConvertLiveFxSpotPriceDomainToResponseModel);
        }

        public FxSpotPrice? ConvertLiveFxSpotPriceEntityToDomain(LiveFxSpotPrice source, FxSpotPrice? destination, ResolutionContext context)
        {
            if (source.FxSpot == null || string.IsNullOrEmpty(source.FxSpot.Pair)) return null;
            return new FxSpotPrice(source.FxSpot.FxSpotId, source.FxSpot.Pair, source.Price, source.LastUpdateTime, FxSpotPriceUpdateEnum.None);
        }

        public ResponseModels.FxSpotPrice? ConvertLiveFxSpotPriceDomainToResponseModel(FxSpotPrice? source, ResponseModels.FxSpotPrice? destination, ResolutionContext context)
        {
            if (source == null) return null;
            return new ResponseModels.FxSpotPrice(
                source.Id, 
                source.Pair, 
                source.Price, 
                source.LastUpdate, 
                (ResponseModels.FxSpotPriceUpdateEnum)Enum.Parse(typeof(FxSpotPriceUpdateEnum), $"{source.LastUpdateDirectionUp}")
            );
        }
    }
}
