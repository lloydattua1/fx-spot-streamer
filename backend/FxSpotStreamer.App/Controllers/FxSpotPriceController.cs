﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using FxSpotStreamer.Core;
using FxSpotStreamer.App.ResponseModels;

namespace FxSpotStreamer.App.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class FxSpotPriceController(
            IMarketDataProvider marketDataProvider,
            ILogger<FxSpotPriceController> logger,
            IMapper mapper
        ) : ControllerBase
    {

        [HttpGet(Name = "GetFxSpotPrices")]
        public IReadOnlyList<FxSpotPrice> Get()
        {
            logger.LogInformation("User is attemping to GetFxSpotPrices from API");
            var prices = mapper.Map<IReadOnlyList<FxSpotPrice>>(marketDataProvider.GetLivePrices());
            logger.LogInformation("User received data from GetFxSpotPrices");
            return prices;
        }
    }
}


