#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS base
WORKDIR /app

EXPOSE 5080
EXPOSE 7099

# Remove comment out if not using this Dockerfile via docker-compose.
# ENV ASPNETCORE_URLS=https://+:7099;http://+:5080


FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
# Install dev cert for the sake of enabling https. In reality, I would use an issued SSL cert.
RUN dotnet dev-certs https -t
WORKDIR /src
COPY . .
RUN dotnet restore "./FxSpotStreamer.App/FxSpotStreamer.App.csproj"

WORKDIR "/src"
RUN dotnet build "./FxSpotStreamer.App/FxSpotStreamer.App.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "./FxSpotStreamer.App/FxSpotStreamer.App.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app

# Dump the dev SSL certificate in the right place
COPY --from=publish /root/.dotnet/corefx/cryptography/x509stores/my/* /root/.dotnet/corefx/cryptography/x509stores/my/
COPY --from=publish /app/publish .

ENTRYPOINT ["dotnet", "FxSpotStreamer.App.dll"]