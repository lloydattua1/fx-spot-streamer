﻿using Microsoft.EntityFrameworkCore;
using FxSpotStreamer.Data.Entities;

namespace FxSpotStreamer.Data
{
    public class DataContext : DbContext
    {
        public virtual DbSet<LiveFxSpotPrice> LiveFxSpotPrices { get; set; }
        public virtual DbSet<FxSpot> FxSpots { get; set; }

        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FxSpot>(entity =>
            {
                entity.ToTable("FxSpot", "dbo");
                entity.Property(e => e.FxSpotId).ValueGeneratedOnAdd();
                entity.Property(e => e.Pair).IsRequired();
                entity.Property(e => e.CreationDate).IsRequired();
            });
            modelBuilder.Entity<LiveFxSpotPrice>(entity =>
            {
                entity.ToTable("LiveFxSpotPrice", "dbo");
                entity.Property(e => e.LiveFxSpotPriceId).ValueGeneratedOnAdd();
                entity.Property(e => e.FxSpotId).IsRequired();
                entity.Property(e => e.Price).IsRequired();
                entity.Property(e => e.LastUpdateTime).IsRequired();
                entity
                .HasOne(e => e.FxSpot)
                .WithMany(e => e.LiveFxSpotPrices)
                .HasForeignKey(e => e.FxSpotId);
            });
        }
    }
}