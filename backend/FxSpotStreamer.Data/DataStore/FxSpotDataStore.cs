﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using FxSpotStreamer.Common.Domain;
using FxSpotStreamer.Common.Data;

namespace FxSpotStreamer.Data.DataStore
{
    public class FxSpotDataStore(
            IServiceProvider serviceProvider, 
            IMapper mapper, 
            ILogger<FxSpotDataStore> logger
        ) : IStaticFxSpotDataStore
    {
        public async Task<IReadOnlyList<FxSpotPrice>> GetFxSpotAsync()
        {
            logger.LogInformation($"User is querying fx spot data");
            try
            {
                using (var scope = serviceProvider.CreateScope())
                {
                    var context = scope.ServiceProvider.GetRequiredService<DataContext>();
                    // Use AsNoTracking to reduce overhead in building model objects
                    var query = await context
                        .LiveFxSpotPrices
                        .Include(s => s.FxSpot)
                        .AsNoTracking()
                        .ToListAsync();
                    logger.LogInformation($"User successfully queried fx spot data.");
                    return mapper.Map<IReadOnlyList<FxSpotPrice>>(query);
                }
            }
            catch (Exception ex)
            {
                logger.LogInformation("User failed to query fx spot data", ex);
                // Perform additional action or cleanup based on business rules on how to deal with exceptions.
                throw;
            }
        }

        public async Task<bool> UpdateFxSpotAsync(IReadOnlyList<FxSpotPrice> fxSpotPrices)
        {
            logger.LogInformation($"Price data is about to be updated...");
            try
            {
                using (var scope = serviceProvider.CreateScope())
                {
                    var pairs = fxSpotPrices.Select(sp => sp.Pair).ToList();
                    var context = scope.ServiceProvider.GetRequiredService<DataContext>();
                    var fxPairNames = await context
                        .LiveFxSpotPrices
                        .Include(lsp => lsp.FxSpot)
                        .Where(lsp => pairs.Contains(lsp.FxSpot.Pair))
                        .ToDictionaryAsync(lsp => lsp.FxSpot.Pair);

                    foreach (var fxSpotPrice in fxSpotPrices)
                    {
                        var thisFxPairEntity = fxPairNames[fxSpotPrice.Pair];
                        thisFxPairEntity.Price = fxSpotPrice.Price;
                        thisFxPairEntity.LastUpdateTime = DateTime.UtcNow;
                    }

                    await context.SaveChangesAsync();
                    logger.LogInformation($"Price data has been updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                logger.LogInformation($"Price data failed to update at {DateTime.UtcNow}", ex);
                // Perform additional action or cleanup based on business rules on how to deal with exceptions.
                throw;
            }
        }
    }
}