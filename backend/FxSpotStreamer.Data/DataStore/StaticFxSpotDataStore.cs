﻿using Microsoft.Extensions.Logging;
using FxSpotStreamer.Common.Domain;
using FxSpotStreamer.Common.Data;

namespace FxSpotStreamer.Data.DataStore
{
    public class StaticFxSpotDataStore(ILogger<StaticFxSpotDataStore> logger) : IStaticFxSpotDataStore
    {
        private IDictionary<string, FxSpotPrice> _fxSpotPrices = new List<FxSpotPrice>()
        {
            new FxSpotPrice(1,"EUR/USD", 1.0740875M, DateTime.UtcNow, FxSpotPriceUpdateEnum.None),
            new FxSpotPrice(2, "USD/GBP",0.78502752M, DateTime.UtcNow, FxSpotPriceUpdateEnum.None),
            new FxSpotPrice(3, "USD/JPY",157.1256M, DateTime.UtcNow, FxSpotPriceUpdateEnum.None),
            new FxSpotPrice(4, "AUD/USD", 0.66076571M, DateTime.UtcNow, FxSpotPriceUpdateEnum.None),
            new FxSpotPrice(5, "USD/CAD", 1.3756208M, DateTime.UtcNow, FxSpotPriceUpdateEnum.None),
            new FxSpotPrice(6, "USD/CNY", 7.2543684M, DateTime.UtcNow, FxSpotPriceUpdateEnum.None),
            new FxSpotPrice(7, "USD/CHF", 0.8975238M, DateTime.UtcNow, FxSpotPriceUpdateEnum.None),
            new FxSpotPrice(8, "USD/HKD", 7.8129703M, DateTime.UtcNow, FxSpotPriceUpdateEnum.None),
            new FxSpotPrice(9, "EUR/GBP", 0.84315871M, DateTime.UtcNow, FxSpotPriceUpdateEnum.None),
            new FxSpotPrice(10, "USD/KRW", 1377.8311M, DateTime.UtcNow, FxSpotPriceUpdateEnum.None)
        }.ToDictionary(k => k.Pair);

        public async Task<IReadOnlyList<FxSpotPrice>> GetFxSpotAsync()
        {
            return await Task.FromResult(_fxSpotPrices.Values.ToList());
        }

        public async Task<bool> UpdateFxSpotAsync(IReadOnlyList<FxSpotPrice> fxSpotPrices)
        {
            return await Task.Run(() =>
            {
                foreach (var fxSpotPrice in fxSpotPrices)
                {
                    if (_fxSpotPrices.ContainsKey(fxSpotPrice.Pair))
                    {
                        _fxSpotPrices[fxSpotPrice.Pair] = fxSpotPrice;
                    }
                    else
                    {
                        logger.LogInformation($"Pair name: {fxSpotPrice.Pair} is unknown! Please check");
                        return false;
                    }                    
                }
                return true;
            });            
        }
    }
}
