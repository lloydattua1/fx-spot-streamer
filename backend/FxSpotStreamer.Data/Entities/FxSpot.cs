﻿namespace FxSpotStreamer.Data.Entities
{
    public class FxSpot
    {
        public int FxSpotId { get; set; }
        public string? Pair { get; set; }
        public DateTime CreationDate { get; set; }
        public virtual IReadOnlyCollection<LiveFxSpotPrice>? LiveFxSpotPrices { get; set; }
    }
}
