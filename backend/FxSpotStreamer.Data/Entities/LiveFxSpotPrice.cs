﻿namespace FxSpotStreamer.Data.Entities
{
    public class LiveFxSpotPrice
    {
        public int LiveFxSpotPriceId { get; set; }
        public int FxSpotId { get; set; }
        public decimal Price { get; set; }
        public DateTime LastUpdateTime { get; set; }
        public virtual FxSpot? FxSpot { get; set; }
    }
}
