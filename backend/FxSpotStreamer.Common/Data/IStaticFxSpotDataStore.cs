﻿using FxSpotStreamer.Common.Domain;

namespace FxSpotStreamer.Common.Data
{
    public interface IStaticFxSpotDataStore
    {
        Task<IReadOnlyList<FxSpotPrice>> GetFxSpotAsync();
        Task<bool> UpdateFxSpotAsync(IReadOnlyList<FxSpotPrice> fxSpotPrices);
    }
}
