﻿namespace FxSpotStreamer.Common.Domain
{
    public class FxSpotPrice
    {
        public int Id { get; set; }
        public string Pair { get; set; }
        public decimal Price { get; set; }
        public DateTime LastUpdate { get; set; }
        public FxSpotPriceUpdateEnum LastUpdateDirectionUp { get; set; }

        public FxSpotPrice(int id, string pair, decimal price, DateTime lastUpdate, FxSpotPriceUpdateEnum lastUpdateDirectionUp)
        {
            Id = id;
            Pair = pair;
            Price = price;
            LastUpdate = lastUpdate;
            LastUpdateDirectionUp = lastUpdateDirectionUp;
        }
    }
}