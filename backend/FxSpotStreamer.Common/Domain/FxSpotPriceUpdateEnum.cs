﻿namespace FxSpotStreamer.Common.Domain
{
    public enum FxSpotPriceUpdateEnum
    {
        Up,
        Down,
        None
    }
}