﻿using MathNet.Numerics.Distributions;
using Microsoft.Extensions.Logging;
using FxSpotStreamer.Common.Domain;

namespace FxSpotStreamer.Core
{
    // You would hook this up to an event manager such as Azure Event Hub, Kafka, RabbitMQ, MQLite, etc
    public class MarketDataEventManager : IMarketDataEventManager
    {
        private readonly int _updateSpeedMilliseconds = 1000;
        private readonly decimal _volatility;
        private readonly ILogger<MarketDataEventManager> logger;
        private readonly IDictionary<string, FxSpotPrice> _pairsToWatch = new Dictionary<string, FxSpotPrice>();
        private readonly IDictionary<string, Normal> _randNumberPerPair = new Dictionary<string, Normal>();
        private readonly SemaphoreSlim _locker = new SemaphoreSlim(1);
        private event Func<IReadOnlyList<FxSpotPrice>, Task>? _fxPairUpdateEventHandler;

        public MarketDataEventManager(ILogger<MarketDataEventManager> logger)
        {
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
            // Use 30% per year volatility and reshape it into volatility per update period
            _volatility = 0.3M * _updateSpeedMilliseconds / 365 / 24 / 60 / 60;
        }

        public void RegisterListener(Func<IReadOnlyList<FxSpotPrice>, Task> actionToExecute, IReadOnlyList<FxSpotPrice> fxPairsToWatch)
        {
            logger.LogInformation("A new listener has registered for events");
            UpdateFxPairsToWatch(fxPairsToWatch);
            _fxPairUpdateEventHandler += actionToExecute;
            // Start listener
            Task.Run(async () => await ListenForPriceUpdatesAsync());
        }

        private async Task ListenForPriceUpdatesAsync()
        {
            // Use a very crude Brownian motion, using a very crude normal distribution. Would never use this in reality, only for the sake of an example.
            while (true)
            {
                foreach (var pair in _pairsToWatch.Values)
                {
                    var newPrice = GetLatestPrice(pair.Price, (decimal)_randNumberPerPair[pair.Pair].Sample());
                    pair.LastUpdateDirectionUp = GetUpdateSign(newPrice, pair.Price);
                    pair.Price = newPrice;
                    pair.LastUpdate = DateTime.UtcNow;

                }
                logger.LogInformation("New prices received. Publishing...");
                if (_fxPairUpdateEventHandler != null)
                {
                    await _fxPairUpdateEventHandler(_pairsToWatch.Values.ToList());
                    logger.LogInformation("New prices published");
                }
                await Task.Delay(_updateSpeedMilliseconds);
            }
        }

        // I would usually create a new class specifically for generating the latest price from a model (to segregate duties
        // and allow testability of this functionality). But leaving it here to simplify the solution as the task is not about
        // generating random price paths.
        private decimal GetLatestPrice(decimal originalPrice, decimal randomNumber)
        {
            return originalPrice + originalPrice * _volatility * randomNumber;
        }

        private FxSpotPriceUpdateEnum GetUpdateSign(decimal newPrice, decimal oldPrice)
        {
            if (newPrice > oldPrice) return FxSpotPriceUpdateEnum.Up;
            if (newPrice < oldPrice) return FxSpotPriceUpdateEnum.Down;
            return FxSpotPriceUpdateEnum.None;
        }

        private void UpdateFxPairsToWatch(IReadOnlyList<FxSpotPrice> fxPairsToWatch)
        {
            foreach (var fxPairToWatch in fxPairsToWatch)
            {
                _pairsToWatch[fxPairToWatch.Pair] = fxPairToWatch;
                if (!_randNumberPerPair.ContainsKey(fxPairToWatch.Pair))
                {
                    _randNumberPerPair[fxPairToWatch.Pair] = Normal.WithMeanStdDev(0, 1);
                }
            }
        }

    }
}