﻿using FxSpotStreamer.Common.Domain;

namespace FxSpotStreamer.Core
{
    public interface IMarketDataProvider
    {
        IReadOnlyList<FxSpotPrice>? GetLivePrices();
        Task InitiateListenerAsync();
    }
}