﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using FxSpotStreamer.Common.Domain;
using FxSpotStreamer.Common.Data;

namespace FxSpotStreamer.Core
{
    // For the sake of simplicity, I have assumed that the list of Fx Pairs being monitored is constant. In reality, I would merge data sets
    // together in case new pairs are fed into the cache.
    // Did not unregister from the event manager because this provider is a singleton. Would unregister if it was scoped or transient style object
    public class MarketDataProvider(
            IHubContext<FxSpotPriceHub> liveFxSpotPricePublisher,
            IMarketDataEventManager marketDataEventManager,
            IServiceProvider serviceProvider,
            IOptions<DataStoreConfig> options,
            ILogger<MarketDataProvider> logger
        ) : IMarketDataProvider
    {
        private readonly SemaphoreSlim _semaphore = new SemaphoreSlim(1);
        IReadOnlyList<FxSpotPrice>? _fxSpotAssetCache = null;

        // Initiate the listener and add pairs to watch
        public async Task InitiateListenerAsync()
        {
            var pairsToWatch = await GetInitialFxSpotPricesAsync();
            marketDataEventManager.RegisterListener(OnMarketDataUpdatedAsync, pairsToWatch);
        }

        public IReadOnlyList<FxSpotPrice>? GetLivePrices()
        {
            return _fxSpotAssetCache;
        }

        private async Task OnMarketDataUpdatedAsync(IReadOnlyList<FxSpotPrice> fxSpotPricePairs)
        {
            logger.LogInformation("Publishing fx price updates to websocket...");
            await liveFxSpotPricePublisher.Clients.All.SendAsync("priceChangedEvent", fxSpotPricePairs);
            await GetFxSpotPriceDataStore().UpdateFxSpotAsync(fxSpotPricePairs);
            await UpdateMarketDataCacheAsync(() => Task.FromResult(fxSpotPricePairs));
        }

        // Set the initial prices in the cache as the most recent values from the data store.
        private async Task<IReadOnlyList<FxSpotPrice>> GetInitialFxSpotPricesAsync()
        {
            return await UpdateMarketDataCacheAsync(() => GetFxSpotPriceDataStore().GetFxSpotAsync());
        }

        private async Task<IReadOnlyList<FxSpotPrice>> UpdateMarketDataCacheAsync(Func<Task<IReadOnlyList<FxSpotPrice>>> getFxSpotPrices)
        {
            if (_fxSpotAssetCache?.Any() != true)
            {
                await _semaphore.WaitAsync();
                try
                {
                    if (_fxSpotAssetCache?.Any() != true)
                    {
                        logger.LogInformation("Updating in-memory cache...");
                        _fxSpotAssetCache = await getFxSpotPrices();
                        logger.LogInformation("In-memory cache updated");
                    }                    
                }
                finally
                {
                    _semaphore.Release();
                }

            }
            return _fxSpotAssetCache;
        }


        private IStaticFxSpotDataStore GetFxSpotPriceDataStore()
        {
            return serviceProvider?
                .CreateScope()
                .ServiceProvider?
                .GetKeyedService<IStaticFxSpotDataStore>(options?.Value?.UseStaticData == true ? "staticDataStore" : "liveDataStore") 
                ?? throw new InvalidOperationException("Unable to get Data Store!");
        }
    }
}