﻿using FxSpotStreamer.Common.Domain;

namespace FxSpotStreamer.Core
{
    // You would hook this up to an event manager such as Azure Event Hub, Kafka, RabbitMQ, MQLite, etc.
    public interface IMarketDataEventManager
    {
        void RegisterListener(Func<IReadOnlyList<FxSpotPrice>, Task> actionToExecute, IReadOnlyList<FxSpotPrice> fxPairsToWatch);
    }
}