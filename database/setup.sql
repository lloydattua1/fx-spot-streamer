CREATE DATABASE FxDataStorage;
GO
USE FxDataStorage;
GO
CREATE TABLE FxSpot (FxSpotId int NOT NULL IDENTITY(1,1) PRIMARY KEY, Pair nvarchar(128) NOT NULL, CreationDate datetime2 NOT NULL);
GO
CREATE TABLE LiveFxSpotPrice (LiveFxSpotPriceId int NOT NULL IDENTITY(1,1) PRIMARY KEY, FxSpotId int NOT NULL, Price decimal(19, 2), LastUpdateTime datetime2 NOT NULL);
GO
INSERT FxSpot SELECT 'EUR/USD', GetDate()
GO
INSERT FxSpot SELECT 'USD/GBP', GetDate()
GO
INSERT FxSpot SELECT 'USD/JPY', GetDate()
GO
INSERT FxSpot SELECT 'AUD/USD', GetDate()
GO
INSERT FxSpot SELECT 'USD/CAD', GetDate()
GO
INSERT FxSpot SELECT 'USD/CNY', GetDate()
GO
INSERT FxSpot SELECT 'USD/CHF', GetDate()
GO
INSERT FxSpot SELECT 'USD/HKD', GetDate()
GO
INSERT FxSpot SELECT 'EUR/GBP', GetDate()
GO
INSERT FxSpot SELECT 'USD/KRW', GetDate()
GO


declare @fxPairId int
set @fxPairId = (select FxSpotId from FxSpot where Pair='EUR/USD')
insert LiveFxSpotPrice select @fxPairId, 1.0740875, GETDATE()
GO
declare @fxPairId2 int
set @fxPairId2 = (select FxSpotId from FxSpot where Pair='USD/GBP')
insert LiveFxSpotPrice select @fxPairId2, 0.78502752, GETDATE()
GO
declare @fxPairId3 int
set @fxPairId3 = (select FxSpotId from FxSpot where Pair='USD/JPY')
insert LiveFxSpotPrice select @fxPairId3, 157.1256, GETDATE()
GO
declare @fxPairId4 int
set @fxPairId4 = (select FxSpotId from FxSpot where Pair='AUD/USD')
insert LiveFxSpotPrice select @fxPairId4, 0.66076571, GETDATE()
GO
declare @fxPairId5 int
set @fxPairId5 = (select FxSpotId from FxSpot where Pair='USD/CAD')
insert LiveFxSpotPrice select @fxPairId5, 1.3756208, GETDATE()
GO
declare @fxPairId6 int
set @fxPairId6 = (select FxSpotId from FxSpot where Pair='USD/CNY')
insert LiveFxSpotPrice select @fxPairId6, 7.2543684, GETDATE()
GO
declare @fxPairId7 int
set @fxPairId7 = (select FxSpotId from FxSpot where Pair='USD/CHF')
insert LiveFxSpotPrice select @fxPairId7, 0.8975238, GETDATE()
GO
declare @fxPairId8 int
set @fxPairId8 = (select FxSpotId from FxSpot where Pair='USD/HKD')
insert LiveFxSpotPrice select @fxPairId8, 7.8129703, GETDATE()
GO
declare @fxPairId9 int
set @fxPairId9 = (select FxSpotId from FxSpot where Pair='EUR/GBP')
insert LiveFxSpotPrice select @fxPairId9, 0.84315871, GETDATE()
GO
declare @fxPairId10 int
set @fxPairId10 = (select FxSpotId from FxSpot where Pair='USD/KRW')
insert LiveFxSpotPrice select @fxPairId10, 1377.8311, GETDATE()
GO